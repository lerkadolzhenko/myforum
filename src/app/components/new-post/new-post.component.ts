import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SharedPostsService } from 'src/app/shared-posts.service';
import { AuthService } from 'src/app/auth.service';


@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})


export class NewPostComponent implements OnInit {
  newPostForm: FormGroup;
  email: string;
  today = new Date().toLocaleString();
  

  constructor(private sharedPostsService: SharedPostsService ,public authService: AuthService) {
    this.newPostForm = this.newPost();
    
  }

  newPost() {
    return new FormGroup({
      userName: new FormControl(),
      title: new FormControl('', [Validators.required]),
      photo: new FormControl('', [Validators.required]),
      postDate: new FormControl(),
      keyWords: new FormControl('', [Validators.required]),
      contentPost: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
    console.log(this.authService.user.subscribe(ref => {
      this.email = ref.email;
      
    }))
  }
  onSubmit(v) {
    v.userName=this.email;
    v.postDate = this.today;
    this.sharedPostsService.addData(v);
  }

}
