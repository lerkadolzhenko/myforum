import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  admin: boolean;
  ngOnInit(): void { }
  email: string;
  password: string;

  constructor(public authService: AuthService , private cd: ChangeDetectorRef) {}

  signup() {
    this.authService.signup(this.email, this.password);
    this.email = this.password = '';
    if (this.email === 'lerka1999dolzhenko@gmail.com') {
      this.admin = true;
    }
  }

  login() {
    this.authService.login(this.email, this.password);
    this.email = this.password = '';    
    if (this.email === 'lerka1999dolzhenko@gmail.com') {
      this.admin = true;
    }
  }

  logout() {
    this.authService.logout();
  }
  
  resetPassword(email: string) {
    this.authService.resetPassword(email)
  }
}