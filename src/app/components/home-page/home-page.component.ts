import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { SharedPostsService } from 'src/app/shared-posts.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  posts;
  key: string;
  postsList: any;
  showSpinner: boolean;
  searchTitle: string = "";
  searchName: string = "";
  searchWords: string = "";


  constructor(public db: AngularFireDatabase, public sharedPostsServise: SharedPostsService) {
    this.loadPosts()
  }

  filterTitle(post) {
    return post.title.toLowerCase().indexOf(this.searchTitle.toLowerCase()) != -1;
  }

  filterAuthor(post) {
    return post.userName.toLowerCase().indexOf(this.searchName.toLowerCase()) != -1;
  }

  filterWords(post) {
    return post.keyWords.toLowerCase().indexOf(this.searchWords.toLowerCase()) != -1;
  }

  filterDateNew(){
    return this.db.list('/newPost').valueChanges().subscribe(post => {
      this.posts=post.reverse();
    })
  }

  filterDateOld(){
    return this.db.list('/newPost').valueChanges().subscribe(post => {
      this.posts=post;
    })
  }

  loadPosts() {
    this.sharedPostsServise.getData(this.key)
      .subscribe(post => {
        console.log(post);

        // if (this.key != post.key  )
        if (this.posts) {
          for (let i = post.length - 2; i >= 0; i--) {
            this.posts.push(post[i]);
          }
        }
        else
          this.posts = post.reverse();
        this.key = this.posts[this.posts.length - 1].key;
        this.showSpinner = false;
      });


  }
  onScroll() {
    // this.amount += this.amount;
    this.loadPosts();
  }


  ngOnInit() {
  }

}
