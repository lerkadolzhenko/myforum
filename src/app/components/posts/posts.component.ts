import { Component, OnInit ,Input} from '@angular/core';
import { SharedPostsService } from 'src/app/shared-posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  searchName: string = "";

  constructor(private postService: SharedPostsService) { }

  ngOnInit() {
  }

  @Input() userName:string;
  @Input() title:string;
  @Input() keyWords:string;
  @Input() photo:string;
  @Input() contentPost:string;
  @Input() postDate:string;
  @Input() key:string;

  
  filterAuthor(post) {
    return post.userName.toLowerCase().indexOf(this.searchName.toLowerCase()) != -1;
  }
}
