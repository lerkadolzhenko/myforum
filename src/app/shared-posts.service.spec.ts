import { TestBed } from '@angular/core/testing';

import { SharedPostsService } from './shared-posts.service';

describe('SharedPostsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedPostsService = TestBed.get(SharedPostsService);
    expect(service).toBeTruthy();
  });
});
